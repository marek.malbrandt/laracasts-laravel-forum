<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ShowThreadReplies extends Component
{
    /** @var \App\Models\Thread */
    public $thread;

    public function render()
    {
        return view('livewire.show-thread-replies');
    }
}
