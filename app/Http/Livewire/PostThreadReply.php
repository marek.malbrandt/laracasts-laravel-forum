<?php

namespace App\Http\Livewire;

use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;
use DanHarrin\LivewireRateLimiting\WithRateLimiting;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Livewire\Component;

class PostThreadReply extends Component
{
    use WithRateLimiting;

    /** @var \App\Models\Thread */
    public $thread;

    public $body = '';

    protected $rules = [
        'body' => 'required|string|min:10|max:1000'
    ];

    public function render()
    {
        return view('livewire.post-thread-reply');
    }

    public function submit()
    {
        try {
            if (!Auth::check()) {
                throw new AuthenticationException();
            }
            $this->validate();

            $userId = auth()->user()->getAuthIdentifier();
            $rateLimitKey = self::class . $this->thread->getKey() . $userId;
            $this->rateLimit(1, 60 * 10, $rateLimitKey);

            $reply = $this->thread->replies()->create([
                'user_id' => $userId,
                'body' => $this->body,
            ]);

            $this->reset(['body']);

            return redirect()->to(URL::previous() . '?#reply-' . $reply->getKey());

        } catch (TooManyRequestsException $exception) {
            $this->resetErrorBag('body');
            $this->addError('body', trans_choice('forum.reply_rate_limit_error_message', $exception->secondsUntilAvailable, [
                'seconds' => $exception->secondsUntilAvailable,
            ]));
            return;
        }
    }
}
