<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Thread extends Component
{
    /** @var \App\Models\Thread */
    public $thread;

    public function render()
    {
        return view('livewire.thread');
    }
}
