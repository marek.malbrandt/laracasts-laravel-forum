<?php

namespace App\Http\Livewire;

use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;
use DanHarrin\LivewireRateLimiting\WithRateLimiting;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class PostThread extends Component
{
    use WithRateLimiting;

    /** @var string */
    public $title;

    /** @var string */
    public $body;

    protected $rules = [
        'title' => 'required|min:20|max:250',
        'body' => 'required|min:500|max:50000',
    ];

    public function render()
    {
        return view('livewire.post-thread');
    }

    public function submit()
    {
        try {
            if (!Auth::check()) {
                throw new AuthenticationException();
            }
            $this->validate();

            $userId = auth()->user()->getAuthIdentifier();
            $rateLimitKey = self::class . $userId;
            $this->rateLimit(1, 60 * 5, $rateLimitKey);

            $thread = \App\Models\Thread::create([
                'user_id' => $userId,
                'title' => $this->title,
                'body' => $this->body,
            ]);

            return redirect()->to('/threads/' . $thread->getRouteKey() . '?');

        } catch (TooManyRequestsException $exception) {
            $this->resetErrorBag();
            $this->addError('body', 'New threads can be posted every 5 minutes. You need to wait ');
            return;
        }
    }
}
