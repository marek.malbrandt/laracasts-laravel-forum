<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Modal extends Component
{
    /** @var string */
    public $id;

    /** @var string */
    public $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.modal');
    }
}
