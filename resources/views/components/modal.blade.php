<div id="{!! $id !!}" class="modal-window">
    <div>
        <a href="#" title="Close" class="modal-close">Close</a>
        <h1 class="text-lg font-bold mt-3 mb-2">{!! $title !!}</h1>
        <div>
            {!! $slot !!}
        </div>
    </div>
</div>
