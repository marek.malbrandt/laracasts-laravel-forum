@php
    /** @var \App\Models\Thread $thread */
    /** @var \App\Models\Reply $reply */
@endphp
<div>
    @foreach($thread->replies as $reply)
        <article class="thread-reply" id="reply-{!! $reply->getKey() !!}">
            <p class="body">{{ $reply->body }}</p>
            <p class="footer">
                <a href="#" class="link">{!! $reply->owner->name !!}</a>
                said
                <abbr class="cursor-help" title="{!! $reply->created_at !!}">
                    {!! $reply->created_at->diffForHumans() !!}
                </abbr>
            </p>
        </article>
    @endforeach
</div>
