<x-modal :id="'post-thread'" :title="'Post a thread'">
    <form action="#" wire:submit.prevent="submit">
        <label for="title" class="block">
            <span class="text-gray-700">Title</span>
            <input type="text"
                   id="title"
                   class="block w-7/12 input"
                   wire:model.defer="title"
                   wire:loading.attr="disabled"
            >
        </label>
        @error('title') <span class="error text-red-600">{{ $message }}</span> @enderror
        <label for="body" class="block mt-4">
            <span class="text-gray-700">Content</span>
            <textarea id="body"
                      class="block w-full input"
                      rows="16"
                      wire:model.defer="body"
                      wire:loading.attr="disabled"
            ></textarea>
        </label>
        @error('body') <span class="error text-red-600">{{ $message }}</span> @enderror
        <div class="block text-right mt-4">
            <button type="submit"
                    wire:loading.attr="disabled"
                    class="btn">
                Submit
            </button>
        </div>
    </form>
</x-modal>
