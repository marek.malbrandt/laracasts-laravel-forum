<div>
    <article class="thread">
        <h2 class="title">
            <a href="{!! $thread->path() !!}" class="link">
                {{ $thread->title }}
            </a>
        </h2>
        <p class="subtitle">posted by <a href="#" class="link">{!! $thread->creator->name !!}</a>
            <abbr class="cursor-help" title="{!! $thread->created_at !!}">
                {!! $thread->created_at->diffForHumans() !!}
            </abbr>
        </p>
        <div class="mt-2">{!! $thread->body !!}</div>
    </article>
</div>
