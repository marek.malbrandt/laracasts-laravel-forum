<div>
    <form wire:submit.prevent="submit" class="grid grid-cols-1 gap-2">
        <div>
            <label for="body w-full">
                <span class="text-gray-700">Your response</span>
                <textarea id="body"
                          class="mt-1 block w-full input bg-gray-50 focus:bg-white hover:cursor-pointer focus:cursor-text"
                          rows="4"
                          wire:model.defer="body"
                          wire:loading.attr="disabled"
                ></textarea>
                @error('body') <span class="error text-red-600">{{ $message }}</span> @enderror
            </label>
        </div>
        <div class="block text-right">
            <button type="submit"
                   wire:loading.attr="disabled"
                   class="btn">
                Submit
            </button>
        </div>
        <div class="block text-right" wire:loading>
            Posting a reply...
        </div>
    </form>
</div>
