<x-app-layout>
    <x-slot name="header">{!! $header !!}</x-slot>

    <div class="container mx-auto lg:px-40 xl:px-72">
        <livewire:thread :thread="$thread" />

        @if($thread->relationLoaded('replies'))
                <div class="block mx-auto px-8 pt-2">
                <h3 class="mb-1">{!! trans_choice('forum.reply', $thread->replies_count, ['count' => $thread->replies_count]) !!}</h3>
                <livewire:show-thread-replies :thread="$thread" />
            </div>
        @endif

        @if(\Illuminate\Support\Facades\Auth::check())
            <div class="mt-12 m-4">
                <livewire:post-thread-reply :thread="$thread" />
            </div>
        @else
            <div>
                <p><a href="/login">Log in</a> in order to post a reply.</p>
            </div>
        @endif
    </div>
</x-app-layout>
