<x-app-layout>
    <x-slot name="header">
        <div>
            <div class="float-left">{!! $header !!}</div>
            @if(\Illuminate\Support\Facades\Auth::check())
                <div class="float-right">
                    <a href="#post-thread" class="btn">Post a thread</a>
                </div>
            @endif
            <div class="clear-both"></div>
        </div>
    </x-slot>
    <div class="container mx-auto lg:px-40 xl:px-72">
        @foreach ($threads as $thread)
            <livewire:thread :thread="$thread"/>
        @endforeach
        @push('modals')
            <livewire:post-thread />
        @endpush
    </div>
</x-app-layout>
