<?php

return [
    'nav' => [
        'threads' => 'Threads',
    ],
    'reply' => '{0} No replies|[1] :count reply|[1,*] :count replies',
    'reply_rate_limit_error_message' => 'Slow down! Please wait another :seconds second to post next reply.|Slow down! Please wait another :seconds seconds to post next reply.'
];
