<?php

namespace Tests\Feature;

use App\Models\Reply;
use App\Models\Thread;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Livewire\Livewire;
use Tests\TestCase;

class ParticipateInThread extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function unathenticated_user_may_not_add_replies()
    {
        $this->expectException(AuthenticationException::class);
        $thread = Thread::factory()->create();

        $reply = Reply::factory()->create();

        $mockLivewire = Livewire::test(\App\Http\Livewire\PostThreadReply::class, compact('thread'));
        $mockLivewire->set('body', $reply->body);
        $mockLivewire->call('submit');
    }

    /** @test */
    public function an_authenticated_user_may_participate_in_forum_threads()
    {
        // Given we have an authenticated user
        $user = $this->logInUser();

        // And an existing thread
        $thread = Thread::factory()->create();

        // When the user adds a reply to the thread
        $reply = Reply::factory()->make();

        // Then their reply should be visible on the page
        $mockLivewire = Livewire::test(\App\Http\Livewire\PostThreadReply::class, compact('thread'));
        $mockLivewire->set('body', $reply->body);
        $mockLivewire->call('submit');

        $this->assertDatabaseHas((new Reply())->getTable(), [
            'thread_id' => $thread->getKey(),
            'user_id' => $user->getAuthIdentifier(),
            'body' => $reply->body,
        ]);
    }
}
