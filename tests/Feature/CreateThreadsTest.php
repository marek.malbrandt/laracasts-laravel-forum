<?php


namespace Feature;


use App\Http\Livewire\PostThread;
use App\Models\Thread;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class CreateThreadsTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /** @test */
    public function an_authenticated_user_can_create_new_forum_threads()
    {
        // Given we have a signed in user
        $user = $this->logInUser();

        // When we submit new thread form
        $fakeThread = Thread::factory()->make();
        $mockLivewire = Livewire::test(PostThread::class);
        $mockLivewire->set('title', $fakeThread->title);
        $mockLivewire->set('body', $fakeThread->body);
        $mockLivewire->call('submit');

        // We should see the new thread
        $this->assertDatabaseHas((new Thread())->getTable(), [
            'user_id' => $user->getKey(),
            'title' => $fakeThread->title,
            'body' => $fakeThread->body,
        ]);
    }
}
