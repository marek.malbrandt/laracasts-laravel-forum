<?php

namespace Tests;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Livewire\Livewire;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function be(UserContract $user, $driver = null)
    {
        parent::be($user, $driver);

        if (!$driver || $driver === 'web') {
            Livewire::actingAs($user);
        }

        return $this;
    }

    /**
     * @param UserContract|null $user
     * @param null $driver
     * @return UserContract|User
     */
    public function logInUser(?UserContract $user = null, $driver = null): UserContract
    {
        if ($user === null) {
            $user = User::factory()->create();
        }

        $this->be($user, $driver);

        return $user;
    }
}
