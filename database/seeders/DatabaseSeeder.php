<?php

namespace Database\Seeders;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Thread::factory(10)->afterCreating(function (Thread $thread) {
            Reply::factory(random_int(1, 5))->create(['thread_id' => $thread->id]);
        })->create();

        User::factory()->create([
            'email' => 'marek.malbrandt@gmail.com',
            'password' => Hash::make('qwe123'),
        ]);
    }
}
